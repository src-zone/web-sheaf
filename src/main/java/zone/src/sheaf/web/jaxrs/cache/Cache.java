/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cache;

/**
 * Annotation to define cache headers for JAX-RS resources. See {@link CacheControlFeature}.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.ws.rs.core.CacheControl;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Repeatable(CacheVariants.class)
public @interface Cache {
    /**
     * @see CacheControl#isPrivate()
     */
    boolean isPrivate() default false;

    /**
     * @see CacheControl#isNoCache()
     */
    boolean isNoCache() default false;

    /**
     * @see CacheControl#isNoStore()
     */
    boolean isNoStore() default false;

    /**
     * @see CacheControl#isNoTransform()
     */
    boolean isNoTransform() default false;

    /**
     * @see CacheControl#isMustRevalidate()
     */
    boolean isMustRevalidate() default false;

    /**
     * @see CacheControl#isProxyRevalidate()
     */
    boolean isProxyRevalidate() default false;

    /**
     * @see CacheControl#getMaxAge()
     */
    int maxAge() default -1;

    /**
     * @see CacheControl#getSMaxAge()
     */
    int sMaxAge() default -1;

    /**
     * Return cache extension control directives in the form "key=value".
     * E.g. <code>&#64;Cache(maxAge = 600, extensions = {"stale-while-revalidate=30"})</code>.
     * An assignment operator ('=') is optional.
     * 
     * @see CacheControl#getCacheExtension()
     */
    String[] extensions() default {};
    
    /**
     * @return The response codes for which this Cache-Control definition is used.
     *         See e.g. <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec13.html#sec13.4">Response Cacheability</a>.
     *         <p>
     *         Note that 304 is also included, because 304 responses MUST generate Cache-Control headers that would have been
     *         sent in a 200 (OK) response. See <a href="https://tools.ietf.org/html/rfc7232#section-4.1">RFC7232 4.1</a>
     */
    int[] validFor() default {200, 203, 206, 300, 301, 304, 410};
}