/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.jsonp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.charset.Charset;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.WriterInterceptor;
import jakarta.ws.rs.ext.WriterInterceptorContext;

import org.glassfish.jersey.message.MessageUtils;

import zone.src.sheaf.web.header.Header;

/**
 * Adds support for {@link Jsonp} to your JAX-RS application.
 */
public class JsonpFeature implements DynamicFeature {
    public static final String APPLICATION_JAVASCRIPT = "application/javascript";
    public static final MediaType APPLICATION_JAVASCRIPT_TYPE = new MediaType("application", "javascript");
    public static final String PARM_CALLBACK = "callback";
    
    @Override public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        Jsonp jsonp = resourceInfo.getResourceMethod().getAnnotation(Jsonp.class);
        if (jsonp != null) {
            if (!resourceInfo.getResourceMethod().isAnnotationPresent(GET.class))
                throw new IllegalArgumentException("@Jsonp annotation is only valid for @GET annotated resources. Offending method: " + resourceInfo.getResourceMethod());
            validateProduces(resourceInfo.getResourceMethod());
            context.register(JsonpMessageBodyWriter.class);
        }
    }

    static void validateProduces(Method resourceMethod) {
        Produces produces = resourceMethod.getAnnotation(Produces.class);
        boolean producesJson = false;
        boolean producesJs = false;
        for (String produce: produces.value()) {
            producesJson = producesJson || MediaType.valueOf(produce).isCompatible(MediaType.APPLICATION_JSON_TYPE);
            producesJs = producesJs || MediaType.valueOf(produce).isCompatible(APPLICATION_JAVASCRIPT_TYPE);
        }
        if (!producesJson)
            throw new IllegalArgumentException("@Jsonp annotation on method that doesn't produce application/json responses. Offending method: "
                    + resourceMethod);
        if (!producesJs)
            throw new IllegalArgumentException("@Jsonp annotation on method that doesn't produce application/javascript responses. Offending method: "
                    + resourceMethod);
    }

    public static class JsonpMessageBodyWriter implements WriterInterceptor {
        @Context UriInfo uriInfo;

        @Override
        public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
            MediaType mediaType = context.getMediaType();
            String callbackName = uriInfo.getQueryParameters().getFirst(PARM_CALLBACK);
            if (callbackName != null && !callbackName.isEmpty() && isCompatible(mediaType)) {
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                OutputStream oldStream = context.getOutputStream();
                try {
                    context.setOutputStream(buffer);
                    // Make sure the messagebodywriter for json is used:
                    context.setMediaType(MediaType.APPLICATION_JSON_TYPE);
                    context.proceed();
                    Charset charset = MessageUtils.getCharset(mediaType);
                    // It's too late to overwrite MediaType, but we can just alter the Content-Type header for the same effect:
                    changeContentTypeToJavascript(context, charset);
                    oldStream.write(callbackName.getBytes(charset));
                    oldStream.write("(".getBytes(charset));
                    oldStream.write(buffer.toByteArray());
                    oldStream.write(")".getBytes(charset));
                } finally {
                    context.setOutputStream(oldStream);
                }
            } else {
                // Make sure it's going to be JSON:
                context.setMediaType(MediaType.APPLICATION_JSON_TYPE);
                context.getHeaders().putSingle(Header.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE);
                context.proceed();
            }
        }

        private void changeContentTypeToJavascript(WriterInterceptorContext context, Charset charset) {
            context.getHeaders().putSingle(Header.CONTENT_TYPE, new MediaType(APPLICATION_JAVASCRIPT_TYPE.getType(), APPLICATION_JAVASCRIPT_TYPE.getSubtype(), charset.name()));
        }

        private boolean isCompatible(MediaType mediaType) {
            return mediaType != null && (mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE) || mediaType.isCompatible(JsonpFeature.APPLICATION_JAVASCRIPT_TYPE));
        }
    }
}