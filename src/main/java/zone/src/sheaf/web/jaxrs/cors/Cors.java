/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to make RESTful resource methods Cross Origin Resource Sharing capable. You need to register
 * {@link CorsFeature} in your JAX-RS application to add support for this annotation. For non-simple requests you may
 * also need {@link CorsModelProcessor}.
 * 
 * {@link CorsFeature} and {@link CorsModelProcessor} will look for resource methods annotated with Cors,
 * or for resource methods annotated with a custom annotation that itself is annotated with Cors.
 * In the latter case, it will take the setting from that indirect annotation. This makes it easier to
 * share the same Cors configuration between different resources, without duplication. 
 */
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
@Retention(value = RetentionPolicy.RUNTIME)
@Inherited
public @interface Cors {
    /**
     * @return False (default) for "Access-Control-Allow-Origin: *". True for only allowing the origin of the
     *         request.
     */
    boolean allowOriginOnly() default false;
    
    /**
     * @return An {@link OriginFilter} implementation that defines which hosts are allowed to have CORS
     *         access to the resource.
     *         This is only used when {@link #allowOriginOnly()} is set to true.
     */
    Class<? extends OriginFilter> originFilter() default AllOriginsFilter.class;

    /**
     * @return True for "Access-Control-Allow-Credentials: true". False (default) for not allowing credentials (cookies)
     *         on CORS requests.
     */
    boolean allowCredentials() default false;

    /**
     * @return The headers to allow via "Access-Control-Allow-Headers" in CORS requests.
     */
    String[] allowHeaders() default {};
    
    /**
     * @return The headers to allow via "Access-Control-Expose-Headers" in CORS requests.
     */
    String[] exposeHeaders() default {};

    /**
     * This setting is for preflighted CORS requests only.
     * 
     * @return The seconds to set for a "Access-Control-Max-Age" header on a preflighted OPTIONS
     *         response, if 0 the header will not be used. See {@link CorsModelProcessor}.
     */
    int maxAge() default 0;
}