/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;

import zone.src.sheaf.web.header.Header;

/**
 * Adds the necessary headers to a response for allowing Cross Origin Request Sharing.
 * For non-simple requests you may need to register {@link CorsModelProcessor} in addition
 * to this feature.
 */
public class CorsFeature implements DynamicFeature {
    @Override public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        Cors cors = getCorsAnnotation(resourceInfo.getResourceMethod());
        if (cors != null)
            context.register(new CorsResponseFilter(cors));
    }

    public static Cors getCorsAnnotation(Method method) {
      Cors cors = method.getAnnotation(Cors.class);
      if (cors == null)
          for (Annotation annotation: method.getAnnotations()) {
              cors = annotation.annotationType().getAnnotation(Cors.class);
              if (cors != null)
                  break;
          }
      return cors;
    }
    
    public static String getHeaders(String[] headers) {
        String result = Arrays.stream(headers).filter(s -> !s.isEmpty()).collect(Collectors.joining(","));
        return result.length() > 0 ? result : null;
    }
    
    private static class CorsResponseFilter implements ContainerResponseFilter {
        private Cors cors;
        private OriginFilter originFilter;
        
        CorsResponseFilter(Cors cors) {
            this.cors = cors;
            try {
                this.originFilter = cors.originFilter().getDeclaredConstructor().newInstance();
            } catch (ReflectiveOperationException e) {
                throw new IllegalArgumentException("Can't instantiate " + cors.originFilter(), e);
            }
        }
        
        @Override
        public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
            String host = requestContext.getHeaderString(Header.ORIGIN);
            if (cors.allowOriginOnly()) {
                // if the request was wrong host may be null, which removes the header
                responseContext.getHeaders().add(Header.ACCESS_CONTROL_ALLOW_ORIGIN, originValidate(originFilter, host));
                responseContext.getHeaders().add(Header.VARY, Header.ORIGIN);
            } else
                responseContext.getHeaders().add(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
            if (cors.allowCredentials())
                responseContext.getHeaders().add(Header.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            String headers = getHeaders(cors.allowHeaders());
            if (headers != null)
                responseContext.getHeaders().add(Header.ACCESS_CONTROL_ALLOW_HEADERS, headers);
            headers = getHeaders(cors.exposeHeaders());
            if (headers != null)
              responseContext.getHeaders().add(Header.ACCESS_CONTROL_EXPOSE_HEADERS, headers);            
        }
    }
    
    public static final String originValidate(OriginFilter filter, String origin) {
        if (filter.allowOrigin(origin))
            return origin;
        return filter.defaultAllowed();
    }
}
