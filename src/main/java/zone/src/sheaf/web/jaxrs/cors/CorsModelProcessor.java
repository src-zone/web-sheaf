/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.Configuration;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.model.ModelProcessor;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.model.ResourceModel;

import zone.src.sheaf.web.header.Header;

/**
 * A model processor that adds resource handler methods for OPTIONS requests on {@link Cors} enabled
 * resources. The resource handler responds with CORS headers for allowing all Cross Origin Request
 * Sharing calls to all {@link Cors} enabled resources.
 * 
 * @see CorsFeature
 */
public class CorsModelProcessor implements ModelProcessor {
    @Override public ResourceModel processResourceModel(ResourceModel resourceModel, Configuration configuration) {
        return processResourceModel(resourceModel, false).build();
    }

    @Override public ResourceModel processSubResource(ResourceModel subResourceModel, Configuration configuration) {
        return processResourceModel(subResourceModel, true).build();
    }

    private ResourceModel.Builder processResourceModel(ResourceModel resourceModel, boolean subResourceModel) {
        ResourceModel.Builder modelBuilder = new ResourceModel.Builder(subResourceModel);
        for (Resource resource: resourceModel.getResources())
            modelBuilder.addResource(processResource(resource));
        return modelBuilder;
    }

    private Resource processResource(Resource resource) {
        Resource.Builder resourceBuilder = Resource.builder(resource.getPath());

        boolean hasOptions = processMethods(resource, resourceBuilder);
        List<CorsInfo> preflighted = hasOptions ? Collections.emptyList() : computePreflighted(resource);
        if (!preflighted.isEmpty()) {
            String allMethods = computeAllMethods(resource);
            createOptionsMethod(resourceBuilder, preflighted, allMethods);
        }

        if (resource.getResourceLocator() != null)
            resourceBuilder.addMethod(resource.getResourceLocator());

        for (Resource child: resource.getChildResources())
            resourceBuilder.addChildResource(processResource(child));
        return resourceBuilder.build();
    }

    private List<CorsInfo> computePreflighted(Resource resource) {
        List<CorsInfo> preflighted = new ArrayList<>();
        for (ResourceMethod resourceMethod: resource.getResourceMethods()) {
            Cors cors = CorsFeature.getCorsAnnotation(resourceMethod.getInvocable().getHandlingMethod());
            if (cors != null)
                preflighted.add(new CorsInfo(resourceMethod.getHttpMethod(), cors));
        }
        return preflighted;
    }
    
    private String computeAllMethods(Resource resource) {
        TreeSet<String> methods = resource.getResourceMethods().stream()
                .map(resourceMethod -> resourceMethod.getHttpMethod())
                .collect(Collectors.toCollection(() -> new TreeSet<>()));
        methods.add(HttpMethod.OPTIONS);
        return methods.stream().collect(Collectors.joining(","));
    }

    private boolean processMethods(Resource resource, Resource.Builder resourceBuilder) {
        boolean hasOptions = false;
        for (ResourceMethod resourceMethod: resource.getResourceMethods()) {
            resourceBuilder.addMethod(resourceMethod);
            if (HttpMethod.OPTIONS.equals(resourceMethod.getHttpMethod())) {
                hasOptions = true;
            }
        }
        return hasOptions;
    }

    private void createOptionsMethod(Resource.Builder resourceBuilder, List<CorsInfo> allowed, String allMethods) {
        resourceBuilder.addMethod(HttpMethod.OPTIONS).handledBy(new OptionsHandler(allowed, allMethods)).consumes(MediaType.WILDCARD).produces(MediaType.WILDCARD).extended(true); 
    }
    
    static String buildAllowCorsHeader(List<CorsInfo> corsInfos) {
        TreeSet<String> methods = corsInfos.stream()
                .map(inf -> inf.getHttpMethod())
                .collect(Collectors.toCollection(() -> new TreeSet<>()));
        return methods.stream().collect(Collectors.joining(","));
    }
    
    static String buildVaryHeader(List<CorsInfo> corsInfos) {
        StringBuilder result = new StringBuilder();
        
        //If one of the methods does allow only the origin, we should have a Vary:Origin on each
        // OPTIONS variant returned:
        for (CorsInfo corsInfo: corsInfos) {
            if (corsInfo.getCors().allowOriginOnly()) {
                result.append(Header.ORIGIN);
                break;
            }
        }
        //If there are variants per Method type requested, we should have a Vary:Access-Control-Request-Method
        // because we return a different response for each method
        if (!isSameCorsForAll(corsInfos)) {
            if (result.length() != 0)
                result.append(',');
            result.append(Header.ACCESS_CONTROL_REQUEST_METHOD);
        }
        return result.length() == 0 ? null : result.toString();
    }
    
    static boolean isSameCorsForAll(List<CorsInfo> corsInfos) {
        if (corsInfos.size() <= 1)
            return true;
        Cors first = corsInfos.get(0).getCors();
        for (int i = 1; i != corsInfos.size(); ++i) {
            Cors other = corsInfos.get(i).getCors();
            if (!first.equals(other))
                return false;
        }
        return true;
    }
    
    private static final class OptionsHandler implements Inflector<ContainerRequestContext, Response> {
        private List<CorsInfo> allowed;
        private String allowHeader;
        private String allowCorsHeader;
        private String varyHeader;
        private boolean sameForAll;
        
        OptionsHandler(List<CorsInfo> allowed, String allMethods) {
            this.allowed = allowed;
            this.allowHeader = allMethods;
            this.allowCorsHeader = buildAllowCorsHeader(allowed);
            this.sameForAll = isSameCorsForAll(allowed);
            this.varyHeader = buildVaryHeader(allowed);
        }
        
        @Override
        public Response apply(ContainerRequestContext containerRequestContext) {
            Response.ResponseBuilder response = Response.noContent();
            if (varyHeader != null)
                response.header(Header.VARY, varyHeader);
            response.header(Header.ALLOW, allowHeader);
            String method = containerRequestContext.getHeaderString(Header.ACCESS_CONTROL_REQUEST_METHOD);
            if (method != null) {
                method = method.trim();
                if (!method.isEmpty() && containerRequestContext.getHeaderString(Header.ORIGIN) != null) {
                    if (sameForAll)
                        return buildCorsResponse(containerRequestContext, response, allowed.get(0), allowCorsHeader);
                    for (CorsInfo corsInfo: allowed)
                        if (corsInfo.getHttpMethod().equals(method))
                            return buildCorsResponse(containerRequestContext, response, corsInfo, method);
                }
            }
            return response.status(Status.NO_CONTENT).build();
        }

        private Response buildCorsResponse(ContainerRequestContext containerRequestContext, Response.ResponseBuilder response, CorsInfo corsInfo, String methods) {
            response.header(Header.ACCESS_CONTROL_ALLOW_ORIGIN, corsInfo.getAllowOrigin(containerRequestContext.getHeaderString(Header.ORIGIN)));
            String allowCredentials = corsInfo.getAllowCredentials();
            if (allowCredentials != null)
                response.header(Header.ACCESS_CONTROL_ALLOW_CREDENTIALS, allowCredentials);
            String allowHeaders = corsInfo.getAllowHeaders();
            if (allowHeaders != null)
                response.header(Header.ACCESS_CONTROL_ALLOW_HEADERS, allowHeaders);
            String exposeHeaders = corsInfo.getExposeHeaders();
            if (exposeHeaders != null)
                response.header(Header.ACCESS_CONTROL_EXPOSE_HEADERS, exposeHeaders);
            int maxAge = corsInfo.getMaxAge();
            if (maxAge > 0)
                response.header(Header.ACCESS_CONTROL_ALLOW_MAX_AGE, maxAge);
            response.header(Header.ACCESS_CONTROL_ALLOW_METHODS, methods);
            return response.status(Status.OK).build();
        }
    }

    private static class CorsInfo {
        private String httpMethod;
        private Cors cors;
        private OriginFilter originFilter;
        
        CorsInfo(String httpMethod, Cors cors) {
            this.httpMethod = httpMethod;
            this.cors = cors;
            try {
                this.originFilter = cors.originFilter().getDeclaredConstructor().newInstance();
            } catch (ReflectiveOperationException e) {
                throw new IllegalArgumentException("Can't instantiate " + cors.originFilter(), e);
            }
        }
        
        public int getMaxAge() {
            return cors.maxAge();
        }
        
        public String getAllowHeaders() {
            return CorsFeature.getHeaders(cors.allowHeaders());
        }
        
        public String getExposeHeaders() {
            return CorsFeature.getHeaders(cors.exposeHeaders());
        }
        
        public String getAllowCredentials() {
            return cors.allowCredentials() ? "true" : null;
        }
        
        public String getAllowOrigin(String origin) {
            if (cors.allowOriginOnly())
                return CorsFeature.originValidate(originFilter, origin);
            return "*";
        }
        
        public String getHttpMethod() {
            return httpMethod;
        }
        
        public Cors getCors() {
            return cors;
        }
    }
}