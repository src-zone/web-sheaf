/*
 * Copyright 2017 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

/**
 * An implementation of {@link OriginFilter} that simply allows all hosts
 * access to the CORS resources. This is used as the default for {@link Cors#originFilter()}
 */
public class AllOriginsFilter implements OriginFilter {
    @Override
    public boolean allowOrigin(String host) {
        return true;
    }
}
