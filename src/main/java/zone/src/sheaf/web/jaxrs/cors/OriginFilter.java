/*
 * Copyright 2017 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

/**
 * This interface can be used as part of {@link Cors#originFilter()} annotations.
 * Implementations are used to define rules for which hosts are allowed access to CORS resources.
 * 
 * @see AllOriginsFilter
 */
public interface OriginFilter {
    /**
     * @param host The value of the Host header in a CORS request
     * @return true if that host is allowed access to the CORS response, false if not.
     */
    boolean allowOrigin(String host);
    
    /**
     * @return The host name to return in the Access-Control-Allow-Origin header if {@link #allowOrigin(String)}
     *         returns false.
     */
    default String defaultAllowed() {
        return "notallowed"; // just return something that won't match a normal host header
    }
}
