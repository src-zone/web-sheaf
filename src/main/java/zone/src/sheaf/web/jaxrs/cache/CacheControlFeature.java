/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cache;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.ext.Provider;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSortedSet;

/**
 * Adds Cache-Control and Vary headers to JAX-RS responses for methods annotated with {@link Cache} and
 * {@link Vary} annotations. It's also possible to define default Cache-Control header responses
 * for any GET method not annotated with {@link Cache}.
 * <p>
 * The {@link Cache} and {@link Vary} annotations can also be added to a custom annotation. Then that custom
 * annotation has the same effect as repeating the original annotations.
 * <p>
 * If a method provides it's own Cache-Control headers before the filter executes, the {@link Cache} annotations
 * will be ignored.
 * <p>
 * Vary headers are added to the response if provided via the {@link Vary} annotation. Vary headers are added
 * when this feature adds Cache-Control headers, and also when the addition of Cache-Control headers was
 * skipped because the resource method already added its own Cache-Control header(s).
 */
@Provider
public class CacheControlFeature implements DynamicFeature {
    private static final Pattern COMMA_SEPARATED_LIST = Pattern.compile("[\\s]*,[\\s]*");
    private static final Set<String> EMPTY_STRING_SET = Collections.emptySet();
    public static final int[] DEFAULT_CACHEABLE_RESPONSES = getCacheDefaults().validFor();
    public static final String[] DEFAULT_VARY = new String[0];
    private CacheControlVariant[] defaultCacheControlVariants;

    /**
     * Initializes this feature to only create headers for methods annotated with @Cache and/or @Vary.
     * Other resource methods are not being filtered.
     */
    public CacheControlFeature() {}
    
    /**
     * Initializes this feature to create headers for methods annotated with @Cache and/or @Vary,
     * and to use the Cache-Control provided as argument for GET methods that do not
     * have a @Cache annotation and match the default {@link Cache#validFor()}.
     * 
     * @param defaultCacheControlForGet The default for not annotated GET methods.
     */
    public CacheControlFeature(CacheControl defaultCacheControlForGet) {
        this.defaultCacheControlVariants = toVariants(defaultCacheControlForGet, DEFAULT_CACHEABLE_RESPONSES);
    }
    
    public boolean hasDefault() {
        return defaultCacheControlVariants != null;
    }

    @Override public void configure(ResourceInfo resourceInfo, FeatureContext featureContext) {
        Method resourceMethod = resourceInfo.getResourceMethod();
        String[] vary = validateVary(resourceMethod);
        CacheVariants cacheVariants = getAnnotation(resourceMethod, CacheVariants.class);
        Cache cache = getAnnotation(resourceMethod, Cache.class);
        if (cacheVariants != null) {
            featureContext.register(new CacheResponseFilter(toVariants(cacheVariants.value()), vary));
        } else if (cache != null) {
            featureContext.register(new CacheResponseFilter(toVariants(cache), vary));
        } else if (defaultCacheControlVariants != null && resourceInfo.getResourceMethod().isAnnotationPresent(GET.class))
            featureContext.register(new CacheResponseFilter(defaultCacheControlVariants, vary));
        else if (getAnnotation(resourceMethod, Vary.class) != null)
            //We don't support only @Vary headers for now. If you want to add Vary headers to your programmatically created
            // Cache-Response headers, use an empty validFor, like: @Cache(validFor = {})
            throw new IllegalStateException("Method with @Vary but no suitable Cache-Control (default or provided by @Cache annotations): "
                    + resourceInfo.getResourceMethod());
    }
    
    static String[] validateVary(Method resourceMethod) {
        Vary vary = getAnnotation(resourceMethod, Vary.class);
        if (vary == null)
            return DEFAULT_VARY;
        for (String varyField: vary.value())
            Preconditions.checkArgument(varyField.indexOf(',') == -1 && varyField.trim().equals(varyField),
                    "Incorrect Vary header field: \"" + vary + "\" in @Vary annotation on: " + resourceMethod);
        return vary.value();
    }

    /**
     * @return a {@link Cache} instances instantiated with the default values.
     */
    @Cache public static Cache getCacheDefaults() {
        try {
            return CacheControlFeature.class.getMethod("getCacheDefaults").getAnnotation(Cache.class);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("This can't happen", e);
        }
    }
    
    private static CacheControlVariant[] toVariants(CacheControl cacheControl, int[] validFor) {
        if (cacheControl == null)
            return null;
        return new CacheControlVariant[] {new CacheControlVariant(cacheControl, validFor)};
    }

    private static CacheControlVariant[] toVariants(Cache cache) {
        return new CacheControlVariant[] {
            new CacheControlVariant(toCacheControl(cache), cache.validFor())
        };
    }

    private static CacheControlVariant[] toVariants(Cache[] cache) {
        Preconditions.checkArgument(cache.length > 0);
        CacheControlVariant[] result = new CacheControlVariant[cache.length];
        for (int i = 0; i != cache.length; ++i)
            result[i] = new CacheControlVariant(toCacheControl(cache[i]), cache[i].validFor());
        return result;
    }

    private static CacheControl toCacheControl(Cache cache) {
        CacheControl result = new CacheControl();
        result.setMaxAge(cache.maxAge());
        result.setMustRevalidate(cache.isMustRevalidate());
        result.setNoCache(cache.isNoCache());
        result.setNoStore(cache.isNoStore());
        result.setNoTransform(cache.isNoTransform());
        result.setPrivate(cache.isPrivate());
        result.setProxyRevalidate(cache.isProxyRevalidate());
        result.setSMaxAge(cache.sMaxAge());
        toCacheControlExtensions(cache, result);
        return result;
    }

    private static void toCacheControlExtensions(Cache cache, CacheControl result) {
        for (String extension: cache.extensions()) {
            String key = extension;
            String value = null;
            int assignIndex = extension.indexOf('=');
            if (assignIndex != -1) {
                value = key.substring(assignIndex + 1).trim();
                key = key.substring(0, assignIndex);
            }
            key = key.trim();
            result.getCacheExtension().put(key, value);
        }
    }
    
    private static final class CacheControlVariant {
        private CacheControl cacheControl;
        private int[] validFor;

        private CacheControlVariant(CacheControl cacheControl, int[] validFor) {
            this.cacheControl = cacheControl;
            this.validFor = sort(validFor); //sorted so we can do binary searches when selecting on a status code
        }

        private int[] sort(int[] source) {
            int result[] = Arrays.copyOf(source, source.length);
            Arrays.sort(result);
            return result;
        }
    }

    private static final class CacheResponseFilter implements ContainerResponseFilter {
        private CacheControlVariant[] cacheControlVariants;
        private String[] vary;

        private CacheResponseFilter(CacheControlVariant[] cacheControlVariants, String[] vary) {
            this.cacheControlVariants = cacheControlVariants;
            this.vary = vary;
        }

        @Override
        public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) {
            CacheControlVariant cacheControlVariant = selectVariant(containerResponseContext);
            if (cacheControlVariant != null) {
                //Cache-Control is only set when the resource methods doesn't provide it's own override:
                if (!containerResponseContext.getHeaders().containsKey(HttpHeaders.CACHE_CONTROL.toLowerCase(Locale.ROOT)))
                    setCacheControlHeader(containerResponseContext, cacheControlVariant);
                //Vary headers are always added:
                addVaryHeaders(containerResponseContext);
            }
        }

        private void addVaryHeaders(ContainerResponseContext containerResponseContext) {
            Collection<String> varyHeaders = getExistingVary(containerResponseContext.getHeaderString(HttpHeaders.VARY));
            for (String varyField: vary)
                if (!varyHeaders.contains(varyField))
                    containerResponseContext.getHeaders().add(HttpHeaders.VARY, varyField);
        }

        private void setCacheControlHeader(ContainerResponseContext containerResponseContext, CacheControlVariant cacheControlVariant) {
            String headerValue = cacheControlVariant.cacheControl.toString();
            if (!headerValue.isEmpty())
                containerResponseContext.getHeaders().putSingle(HttpHeaders.CACHE_CONTROL, headerValue);
        }

        private Collection<String> getExistingVary(String existingVary) {
            if (existingVary != null && !existingVary.isEmpty())
                return ImmutableSortedSet.orderedBy(String.CASE_INSENSITIVE_ORDER)
                        .add(COMMA_SEPARATED_LIST.split(existingVary.trim())).build();
            return EMPTY_STRING_SET;
        }

        private CacheControlVariant selectVariant(ContainerResponseContext containerResponseContext) {
            int status = containerResponseContext.getStatus();
            for (CacheControlVariant variant: cacheControlVariants)
                if (Arrays.binarySearch(variant.validFor, status) >= 0)
                    return variant;
            return null;
        }
    }
    
    public static <T extends Annotation> T getAnnotation(Method method, Class<T> annotationType) {
      T result = method.getAnnotation(annotationType);
      if (result == null)
          for (Annotation annotation: method.getAnnotations()) {
              result = annotation.annotationType().getAnnotation(annotationType);
              if (result != null)
                  break;
          }
      return result;
    }
}