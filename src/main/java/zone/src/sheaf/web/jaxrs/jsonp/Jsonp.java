/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.jsonp;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.ws.rs.Produces;

/**
 * Put this annotation on a JAX-RS resource method together with a {@link Produces} annotation for both
 * application/json and application/javascript to enable JSONP results when a callback parameter (named
 * "callback", see {@link JsonpFeature#PARM_CALLBACK}) is provided as query parameter on the request.
 * <p>
 * Only supported for GET methods.
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
@Inherited
public @interface Jsonp {}