/*
 * Copyright 2017 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.header;

public interface Headers {
    /**
     * Returns the value of the specified header by name.
     * If a header with the specified name is not provided,
     * this method returns <code>null</code>.
     * If there are multiple headers with the same name, this method
     * returns the value of the first added.
     * The header name is case insensitive.
     *
     * @param name The name of the header
     * @return The value of the first matching header, or null
     */         
    String getHeaderValue(String name);
}
