/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.header;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import com.google.common.base.Preconditions;

public class Header {
    public static final String ACCEPT = "Accept";
    
    //CORS Request headers:
    public static final String ORIGIN = "Origin";
    public static final String ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method"; // (preflight request: what will be used in actual request)
    public static final String ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers"; // (preflight request: what will be used in actual request)
    
    //CORS Response Headers:
    public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    public static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods"; // (preflight OPTIONS)
    public static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    public static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
    public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    public static final String ACCESS_CONTROL_ALLOW_MAX_AGE = "Access-Control-Max-Age"; // (preflight OPTIONS)
    
    public static final String VARY = "Vary";
    
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String LAST_MODIFIED = "Last-Modified";
    public static final String TRANSFER_ENCODING = "Transfer-Encoding";
    
    public static final String ALLOW = "Allow";
    public static final String DATE = "Date";
    public static final String SERVER = "Server";
    public static final String USER_AGENT = "User-Agent";
    
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String ETAG = "ETag";
    public static final String IF_NONE_MATCH = "If-None-Match";
    
    public static final String LOCATION = "Location";
    
    public static final int SECONDS_IN_YEAR = 31556926;
    public static final String MAX_AGE_1YEAR = "max-age=" + SECONDS_IN_YEAR;
    
    public static final Header HEADER_CACHE_NO_STORE_NO_CACHE_MUST_REVALIDATE = new Header(CACHE_CONTROL, "no-store, no-cache, must-revalidate");
    public static final Header HEADER_CACHE_FAR_FUTURE_EXPIRES = new Header(CACHE_CONTROL, MAX_AGE_1YEAR);
    public static final Header HEADER_CACHE_1H = new Header(Header.CACHE_CONTROL, "max-age=3600");
    public static final Header EXPIRES_0 = new Header("Expires", "0");
    
    private String name;
    private String[] values;
    
    public Header(String name, String... values) {
        this.name = name;
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public Iterable<String> getValues() {
        return Arrays.asList(values);
    }
    
    /**
     * Encode a value according to <a href="https://tools.ietf.org/html/rfc5987">RFC 5987</a>
     * (e.g. for use in a Content-Disposition filename header value
     * @param value the header value to encode
     * @param charset the character set to use for the encoding
     * @return the RFC 5987 encoded value
     */
    public static String encodeHeaderParmValue(String input, Charset charset) {
        Preconditions.checkNotNull(input);
        Preconditions.checkNotNull(charset);
        // Currently we only support UTF-8 (the RFC is also for ISO-8859-1)
        Preconditions.checkArgument(StandardCharsets.UTF_8.equals(charset));        
        byte[] bytes = input.getBytes(charset);
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        sb.append(charset.name());
        sb.append("''");
        for (byte b: bytes) {
            if (isAttrChar(b))
                sb.append((char) b);
            else {
                sb.append('%');
                sb.append(Character.toUpperCase(Character.forDigit((b >> 4) & 0xF, 16)));
                sb.append(Character.toUpperCase(Character.forDigit(b & 0xF, 16)));
            }
        }
        return sb.toString();
    }

    @SuppressWarnings("checkstyle:CyclomaticComplexity")
    private static boolean isAttrChar(byte c) {
        return
                (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') // ALPHA | DIGIT
                || c == '!' || c == '#' || c == '$' || c == '&' || c == '+' || c == '-' || c == '.'
                || c == '^' || c == '_' || c == '`' || c == '|' || c == '~';
    }
}