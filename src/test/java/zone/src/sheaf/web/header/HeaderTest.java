/*
 * Copyright 2016 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.header;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

public class HeaderTest {
    @Test public void encodeHeaderParmValue() {
        assertThat(Header.encodeHeaderParmValue("value", StandardCharsets.UTF_8)).isEqualTo("UTF-8''value");
        assertThat(Header.encodeHeaderParmValue("value #$%^&`~", StandardCharsets.UTF_8)).isEqualTo("UTF-8''value%20#$%25^&`~");
        assertThatThrownBy(() -> {Header.encodeHeaderParmValue("value", StandardCharsets.ISO_8859_1);}).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> {Header.encodeHeaderParmValue("value", StandardCharsets.US_ASCII);}).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> {Header.encodeHeaderParmValue("value", null);}).isInstanceOf(NullPointerException.class);
        assertThatThrownBy(() -> {Header.encodeHeaderParmValue(null, StandardCharsets.UTF_8);}).isInstanceOf(NullPointerException.class);
    }
}
