/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.inmemory.InMemoryTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

import zone.src.sheaf.web.header.Header;

public class CorsFeatureTest extends JerseyTest {
    @BeforeEach public void setUp() throws Exception {
        super.setUp();
    }

    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    @Override protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new InMemoryTestContainerFactory();
    }

    @Override protected Application configure() {
        return new ResourceConfig(CorsFeature.class, StubResource.class, AllowFilterResource.class, ErrorResource.class, ErrorMapper.class)
                .addProperties(ImmutableMap.of(
                    ServerProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true
        ));
    }
    
    @Test public void test() {
        verifyRequestResponse(StubResource.PATH, "http://localhost", HttpMethod.DELETE, null,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*"));
        verifyRequestResponse(StubResource.PATH, "http://localhost", HttpMethod.PUT, Entity.text(""),
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "http://localhost",
                        Header.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true",
                        Header.ACCESS_CONTROL_ALLOW_HEADERS, "X-MY-HEADER1,X-MY-HEADER2",
                        Header.ACCESS_CONTROL_EXPOSE_HEADERS, "X-EXPOSED",
                        Header.VARY, Header.ORIGIN));
    }
    
    @Test public void error() {
        verifyRequestResponse(ErrorResource.PATH, "http://localhost", HttpMethod.GET, null,
                Status.INTERNAL_SERVER_ERROR.getStatusCode(), ImmutableMap.of(
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*"));  
    }
    
    @Test public void allowFilter() {
        verifyRequestResponse(AllowFilterResource.PATH, "http://localhost", HttpMethod.GET, null,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "https://notallowed.com",
                        Header.VARY, Header.ORIGIN));
        verifyRequestResponse(AllowFilterResource.PATH, null, HttpMethod.GET, null,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "https://notallowed.com",
                        Header.VARY, Header.ORIGIN));
        verifyRequestResponse(AllowFilterResource.PATH, "http://allowed.com", HttpMethod.GET, null,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "http://allowed.com",
                        Header.VARY, Header.ORIGIN));
    }
     
    public Response verifyRequestResponse(String path, String origin, String method, Entity<?> entity, int status, Map<String, String> headers) {
        Invocation.Builder responseBuilder = target(path).request();
        if (origin != null)
            responseBuilder.header(Header.ORIGIN, origin);
        Response response = responseBuilder.method(method, entity);
        assertThat(response.getStatus()).isEqualTo(status);
        for (String name: headers.keySet())
            assertThat(response.getHeaderString(name)).isEqualTo(headers.get(name));
        List<String> allHeaders = new ArrayList<>(headers.keySet());
        allHeaders.add(Header.CONTENT_LENGTH);
        assertThat(response.getHeaders().keySet()).containsOnly(allHeaders.toArray(new String[allHeaders.size()]));
        return response;
    }
    
    @Cors(allowCredentials = true, allowOriginOnly = true, maxAge = 1000,
        allowHeaders = {"X-MY-HEADER1", "X-MY-HEADER2"},
        exposeHeaders = {"X-EXPOSED"})
    @Target({ ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
    @Retention(value = RetentionPolicy.RUNTIME)
    @Inherited
    public @interface CorsConfiguration {
    }
    
    @Path(StubResource.PATH)
    public static class StubResource {
        static final String PATH = "stub";
        
        @GET
        public Response get() {
            return Response.ok().build();
        }

        @PUT
        @CorsConfiguration
        public Response put(String entity) {
            return Response.ok().build();
        }
        
        @DELETE
        @Cors
        public Response delete() {
            return Response.ok().build();
        }
    }
    
    @Path(AllowFilterResource.PATH)
    public static class AllowFilterResource {
        static final String PATH = "allowFilter";
        
        @GET
        @Cors(allowOriginOnly = true, originFilter = TestOriginFilter.class)
        public Response get() {
            return Response.ok().build();
        }
    }
    
    @Path(ErrorResource.PATH)
    public static class ErrorResource {
        static final String PATH = "error";
        
        @GET
        @Cors
        public Response get() {
            throw new IllegalStateException("This should trigger the ErrorMapper");
        }
    }
    
    public static class ErrorMapper implements ExceptionMapper<IllegalStateException> {
        @Override
        public Response toResponse(IllegalStateException exception) {
            return Response.status(500).build();
        }
    }
    
    public static class TestOriginFilter implements OriginFilter {
        @Override
        public boolean allowOrigin(String host) {
            return "http://allowed.com".equalsIgnoreCase(host);
        }

        @Override
        public String defaultAllowed() {
            return "https://notallowed.com";
        }
    }
}
