/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class AllOriginsFilterTest {
    @Test public void allowOrigin() {
        AllOriginsFilter filter = new AllOriginsFilter();
        assertThat(filter.allowOrigin("https://example.com")).isTrue();
        assertThat(filter.allowOrigin("http://anotherhost.zone")).isTrue();
    }
    
    @Test public void defaultAllowed() {
        AllOriginsFilter filter = new AllOriginsFilter();
        assertThat(filter.defaultAllowed()).isEqualTo("notallowed");
    }
}
