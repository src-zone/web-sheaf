/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cors;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.inmemory.InMemoryTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

import zone.src.sheaf.web.header.Header;
import zone.src.sheaf.web.jaxrs.cors.CorsFeatureTest.TestOriginFilter;

public class CorsModelProcessorTest extends JerseyTest {
    @BeforeEach public void setUp() throws Exception {
        super.setUp();
    }

    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    @Override
    protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new InMemoryTestContainerFactory();
    }
    
    @Override
    protected Application configure() {
        return new ResourceConfig(
                    CorsModelProcessor.class, Stub0Resource.class, Stub1Resource.class, Stub2Resource.class, Stub3Resource.class,
                    StubWithOptions.class, StubAllowedHostsFilter.class)
                .addProperties(ImmutableMap.of(
                    //Disable WADL as it also responds to OPTIONS requests:
                    ServerProperties.WADL_FEATURE_DISABLE, true,
                    ServerProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true
        ));
    }
    
    @Test
    public void zeroPreflight() {
        Response response = target(Stub0Resource.PATH).request()
                .header(Header.ORIGIN, "http://localhost")
                .header(Header.ACCESS_CONTROL_REQUEST_METHOD, HttpMethod.PUT)
                .options();
        //Jersey automatically responds to OPTIONS with an Allow response header for the set of HTTP methods supported
        // by the resource:
        assertThat(response.getHeaders().keySet()).containsOnly(Header.CONTENT_TYPE, Header.CONTENT_LENGTH, Header.ALLOW);
    }
 
    @Test
    public void onePreflight() {
        verifyPreflightResponse(Stub1Resource.PATH, "http://localhost", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ALLOW, "OPTIONS,PUT",
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*",
                        Header.ACCESS_CONTROL_ALLOW_METHODS, HttpMethod.PUT));  
    }
    
    @Test
    public void missingMethod() {
        verifyPreflightResponse(Stub1Resource.PATH, "http://localhost", null,
                Status.NO_CONTENT.getStatusCode(), ImmutableMap.of(
                        Header.ALLOW, "OPTIONS,PUT"));  
    }
    
    @Test
    public void missingOrigin() {
        verifyPreflightResponse(Stub1Resource.PATH, null, HttpMethod.PUT,
                Status.NO_CONTENT.getStatusCode(), ImmutableMap.of(
                        Header.ALLOW, "OPTIONS,PUT"));  
    }
    
    @Test
    public void multiplePreflight() {
        verifyPreflightResponse(Stub2Resource.PATH, "http://localhost", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.<String,String>builder()
                    .put(Header.ALLOW, "DELETE,OPTIONS,PUT")
                    .put(Header.VARY, "Origin,Access-Control-Request-Method")
                    .put(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "http://localhost")
                    .put(Header.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true")
                    .put(Header.ACCESS_CONTROL_ALLOW_METHODS, HttpMethod.PUT)
                    .put(Header.ACCESS_CONTROL_ALLOW_HEADERS, "X-MY-HEADER1,X-MY-HEADER2")
                    .put(Header.ACCESS_CONTROL_EXPOSE_HEADERS, "X-EXPOSED")
                    .put(Header.ACCESS_CONTROL_ALLOW_MAX_AGE, "1000").build());
        
        verifyPreflightResponse(Stub2Resource.PATH, "http://localhost", HttpMethod.DELETE,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ALLOW, "DELETE,OPTIONS,PUT",
                        Header.VARY, "Origin,Access-Control-Request-Method",
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*",
                        Header.ACCESS_CONTROL_ALLOW_METHODS, HttpMethod.DELETE));
        
        verifyPreflightResponse(Stub2Resource.PATH, "http://localhost", HttpMethod.POST,
                Status.NO_CONTENT.getStatusCode(), ImmutableMap.of(
                        Header.ALLOW, "DELETE,OPTIONS,PUT",
                        Header.VARY, "Origin,Access-Control-Request-Method"));
    }
    
    @Test
    public void multiplePreflightSameCors() {
        verifyPreflightResponse(Stub3Resource.PATH, "http://localhost", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.<String,String>builder()
                    .put(Header.ALLOW, "DELETE,OPTIONS,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
                    .put(Header.ACCESS_CONTROL_ALLOW_METHODS, "DELETE,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_HEADERS, "X-MY-HEADER1,X-MY-HEADER2")
                    .put(Header.ACCESS_CONTROL_ALLOW_MAX_AGE, "1000").build());
        
        verifyPreflightResponse(Stub3Resource.PATH, "http://localhost", HttpMethod.DELETE,
                Status.OK.getStatusCode(), ImmutableMap.<String,String>builder()
                    .put(Header.ALLOW, "DELETE,OPTIONS,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
                    .put(Header.ACCESS_CONTROL_ALLOW_METHODS, "DELETE,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_HEADERS, "X-MY-HEADER1,X-MY-HEADER2")
                    .put(Header.ACCESS_CONTROL_ALLOW_MAX_AGE, "1000").build());
        
        verifyPreflightResponse(Stub3Resource.PATH, "http://localhost", HttpMethod.POST,
                Status.OK.getStatusCode(), ImmutableMap.<String,String>builder()
                    .put(Header.ALLOW, "DELETE,OPTIONS,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
                    .put(Header.ACCESS_CONTROL_ALLOW_METHODS, "DELETE,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_HEADERS, "X-MY-HEADER1,X-MY-HEADER2")
                    .put(Header.ACCESS_CONTROL_ALLOW_MAX_AGE, "1000").build());
    }
    
    @Test
    public void subResource() {
        verifyPreflightResponse(Stub2Resource.PATH + "/" + Stub2Resource.PATH_SUB, "http://localhost", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.ALLOW, "OPTIONS,PUT",
                        Header.ACCESS_CONTROL_ALLOW_ORIGIN, "*",
                        Header.ACCESS_CONTROL_ALLOW_METHODS, HttpMethod.PUT));
    }
    
    @Test
    public void dontOverrideOptions() {
        Response response = verifyPreflightResponse(StubWithOptions.PATH, "http://localhost", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.of(
                        Header.VARY, "XYZ",
                        Header.CONTENT_TYPE, MediaType.TEXT_PLAIN));
        assertThat(response.readEntity(String.class)).isEqualTo("original options");
    }
    
    @Test
    public void allowOrigin() {
        verifyPreflightResponse(StubAllowedHostsFilter.PATH, "http://allowed.com", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.<String,String>builder()
                    .put(Header.VARY, Header.ORIGIN)
                    .put(Header.ALLOW, "OPTIONS,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "http://allowed.com")
                    .put(Header.ACCESS_CONTROL_ALLOW_METHODS, "PUT").build());
        
        verifyPreflightResponse(StubAllowedHostsFilter.PATH, "http://localhost", HttpMethod.PUT,
                Status.OK.getStatusCode(), ImmutableMap.<String,String>builder()
                    .put(Header.VARY, Header.ORIGIN)
                    .put(Header.ALLOW, "OPTIONS,PUT")
                    .put(Header.ACCESS_CONTROL_ALLOW_ORIGIN, "https://notallowed.com")
                    .put(Header.ACCESS_CONTROL_ALLOW_METHODS, "PUT").build());
    }
    
    public Response verifyPreflightResponse(String path, String origin, String method, int status, Map<String, String> headers) {
        Invocation.Builder responseBuilder = target(path).request();
        if (origin != null)
            responseBuilder.header(Header.ORIGIN, origin);
        if (method != null)
            responseBuilder.header(Header.ACCESS_CONTROL_REQUEST_METHOD, method);
        Response response = responseBuilder.options();
        assertThat(response.getStatus()).isEqualTo(status);
        for (String name: headers.keySet())
            assertThat(response.getHeaderString(name)).isEqualTo(headers.get(name));
        List<String> allHeaders = new ArrayList<>(headers.keySet());
        allHeaders.add(Header.CONTENT_LENGTH);
        assertThat(response.getHeaders().keySet()).containsOnly(allHeaders.toArray(new String[allHeaders.size()]));
        return response;
    }
    
    @Path(Stub0Resource.PATH)
    public static class Stub0Resource {
        static final String PATH = "stub0";

        @PUT
        public Response put() {
            return Response.ok().build();
        }
    }
    
    @Path(Stub1Resource.PATH)
    public static class Stub1Resource {
        static final String PATH = "stub1";

        @PUT
        @Cors
        public Response put() {
            return Response.ok().build();
        }
    }
    
    @Path(Stub2Resource.PATH)
    public static class Stub2Resource {
        static final String PATH = "stub2";
        static final String PATH_SUB = "sub";

        @PUT
        @Cors(allowCredentials = true, allowOriginOnly = true, maxAge = 1000,
            allowHeaders = {"X-MY-HEADER1", "X-MY-HEADER2"}, exposeHeaders = {"X-EXPOSED"})
        public Response put() {
            return Response.ok().build();
        }
        
        @DELETE
        @Cors
        public Response delete() {
            return Response.ok().build();
        }
        
        @Path(PATH_SUB)
        public SubResource getItemContentResource() {
            return new SubResource();
        }
    }
    
    @Cors(allowHeaders = {"X-MY-HEADER1", "X-MY-HEADER2"}, maxAge = 1000)
    @Target({ ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
    @Retention(value = RetentionPolicy.RUNTIME)
    @Inherited
    public @interface CorsConfiguration {
    }
    
    @Path(Stub3Resource.PATH)
    public static class Stub3Resource {
        static final String PATH = "stub3";

        @PUT
        @CorsConfiguration
        public Response put() {
            return Response.ok().build();
        }
        
        @DELETE
        @Cors(allowHeaders = {"X-MY-HEADER1", "X-MY-HEADER2"}, maxAge = 1000)
        public Response delete() {
            return Response.ok().build();
        }
    }
    
    public static class SubResource {
        @PUT
        @Cors
        public Response put() {
            return Response.ok().build();
        }
    }
    
    @Path(StubWithOptions.PATH)
    public static class StubWithOptions {
        static final String PATH = "stubWithOptions";

        @PUT
        @Cors
        public Response put() {
            return Response.ok().build();
        }
        
        @OPTIONS
        @Cors
        public Response options() {
            return Response.ok("original options").header(Header.VARY, "XYZ").build();
        }
    }
    
    @Path(StubAllowedHostsFilter.PATH)
    public static class StubAllowedHostsFilter {
        static final String PATH = "allowFilter";
        
        @PUT
        @Cors(allowOriginOnly = true, originFilter = TestOriginFilter.class)
        public Response put() {
            return Response.ok().build();
        }
    }
    
    @Path(ErrorResource.PATH)
    public static class ErrorResource {
        static final String PATH = "error";
        
        @GET
        @Cors
        public Response put() {
            throw new IllegalStateException("This should trigger the ErrorMapper");
        }
    }
    
    public static class ErrorMapper implements ExceptionMapper<IllegalStateException> {
        @Override
        public Response toResponse(IllegalStateException exception) {
            return Response.status(500).build();
        }
    }
}