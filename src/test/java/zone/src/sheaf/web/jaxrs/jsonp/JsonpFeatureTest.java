/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.jsonp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.inmemory.InMemoryTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;

public class JsonpFeatureTest extends JerseyTest {
    private static final int OK = Status.OK.getStatusCode();
    private static final int NOT_ACCEPTABLE = Status.NOT_ACCEPTABLE.getStatusCode();
    private static final MediaType APPLICATION_JAVASCRIPT_UTF8 = new MediaType("application", "javascript", StandardCharsets.UTF_8.name());
    
    @BeforeEach public void setUp() throws Exception {
        super.setUp();
    }

    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    @Override protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new InMemoryTestContainerFactory();
    }
    
    @Override protected Application configure() {
        return new ResourceConfig(JsonpFeature.class, StubResource.class, ErrorResource.class, ErrorMapper.class);
    }
    
    @Test public void get() throws JSONException {
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "*/*", "eval_1",
                OK, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "application/*", "eval_1",
                OK, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        // TODO When no callback is given but javascript requested we return json (should be NOT_ACCEPTABLE):
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "application/javascript", null,
                OK, MediaType.APPLICATION_JSON_TYPE, "{\"field\":\"value\"}");
        // TODO When no callback is given but javascript requested we return json (should be NOT_ACCEPTABLE):
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "application/javascript", "",
                OK, MediaType.APPLICATION_JSON_TYPE, "{\"field\":\"value\"}");
        // TODO When a callback is given but json requested we return javascript (should be NOT_ACCEPTABLE):
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "application/json", "eval_2",
                OK, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "application/javascript", "eval_3",
                OK, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        verifyRequestResponse(StubResource.PATH, HttpMethod.GET, "text/plain", "eval_3",
                NOT_ACCEPTABLE, null, null);
    }
    
    @Test public void getError() throws JSONException {
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "*/*", "eval_1",
                500, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "application/*", "eval_1",
                500, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        // TODO When no callback is given but javascript requested we return json (should be NOT_ACCEPTABLE):
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "application/javascript", null,
                500, MediaType.APPLICATION_JSON_TYPE, "{\"field\":\"value\"}");
        // TODO When no callback is given but javascript requested we return json (should be NOT_ACCEPTABLE):
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "application/javascript", "",
                500, MediaType.APPLICATION_JSON_TYPE, "{\"field\":\"value\"}");
        // TODO When a callback is given but json requested we return javascript (should be NOT_ACCEPTABLE):
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "application/json", "eval_2",
                500, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "application/javascript", "eval_3",
                500, APPLICATION_JAVASCRIPT_UTF8, "{\"field\":\"value\"}");
        verifyRequestResponse(ErrorResource.PATH, HttpMethod.GET, "text/plain", "eval_3",
                NOT_ACCEPTABLE, null, null);
    }
    
    @Test public void delete() throws JSONException {
        //Only GET requests support JSON-P:
        verifyRequestResponse(StubResource.PATH, HttpMethod.DELETE, "*/*", "eval_1",
                OK, MediaType.APPLICATION_JSON_TYPE, "{\"field\":\"value\"}");
    }
    
    @Test public void validateProduces() throws NoSuchMethodException, SecurityException {
        assertThatThrownBy(() -> {JsonpFeature.validateProduces(JsonpFeatureTest.class.getMethod("onlyJson"));}).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> {JsonpFeature.validateProduces(JsonpFeatureTest.class.getMethod("onlyJs"));}).isInstanceOf(IllegalArgumentException.class);
        
        //valid:
        JsonpFeature.validateProduces(JsonpFeatureTest.class.getMethod("specialized"));
    }
    
    @Test public void notGet() throws NoSuchMethodException, SecurityException {
        ResourceInfo resourceInfo = mock(ResourceInfo.class);
        when(resourceInfo.getResourceMethod()).thenReturn(JsonpFeatureTest.class.getMethod("post"));
        FeatureContext context = mock(FeatureContext.class);
        assertThatThrownBy(() -> {new JsonpFeature().configure(resourceInfo, context);}).isInstanceOf(IllegalArgumentException.class);
    }
    
    @GET @Jsonp() @Produces({MediaType.APPLICATION_JSON}) public void onlyJson() {
        //dummy method
    }
    @GET @Jsonp() @Produces({JsonpFeature.APPLICATION_JAVASCRIPT})  public void onlyJs() {
        //dummy method
    }
    @GET @Jsonp() @Produces({"application/javascript;charset=utf-8", MediaType.APPLICATION_JSON})  public void specialized() {
        //dummy method
    }
    @POST @Jsonp() @Produces({JsonpFeature.APPLICATION_JAVASCRIPT, MediaType.APPLICATION_JSON})  public void post() {
        //dummy method
    }
     
    public Response verifyRequestResponse(String path, String method, String accept, String callback, int status, MediaType type, String json) throws JSONException {
        Invocation.Builder requestBuilder = target(path).queryParam("callback", callback).request();
        requestBuilder.accept(accept);
        Response response = requestBuilder.method(method);
        assertThat(response.getStatus()).isEqualTo(status);
        assertThat(response.getMediaType()).isEqualTo(type);
        String content = response.readEntity(String.class);
        if (JsonpFeature.APPLICATION_JAVASCRIPT_TYPE.isCompatible(type)) {
            assertThat(content).startsWith(callback + "(");
            assertThat(content).endsWith(")");
            content = content.substring(callback.length() + 1, content.length() - 1);
        }
        if (json != null)
            JSONAssert.assertEquals(json, content, true);
        else
            assertThat(content).isEmpty();
        return response;
    }
    
    public static class DummyObject {
        String field;
        
        public DummyObject() {
        }
        
        public DummyObject(String field) {
            this.field = field;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }
    }
    
    @Path(StubResource.PATH)
    public static class StubResource {
        static final String PATH = "stub";
        
        @GET
        @Jsonp
        @Produces({MediaType.APPLICATION_JSON, JsonpFeature.APPLICATION_JAVASCRIPT})
        public DummyObject get() {
            return new DummyObject("value");
        }
        
        @DELETE
        @Produces({MediaType.APPLICATION_JSON, JsonpFeature.APPLICATION_JAVASCRIPT})
        public DummyObject delete() {
            return new DummyObject("value");
        }
    }
    
    @Path(ErrorResource.PATH)
    public static class ErrorResource {
        static final String PATH = "error";
        
        @GET
        @Jsonp
        @Produces({MediaType.APPLICATION_JSON, JsonpFeature.APPLICATION_JAVASCRIPT})
        public DummyObject get() {
            throw new IllegalStateException("This should trigger the ErrorMapper");
        }
    }
    
    public static class ErrorMapper implements ExceptionMapper<IllegalStateException> {
        @Override
        public Response toResponse(IllegalStateException exception) {
            return Response.status(500).entity(new DummyObject("value")).build();
        }
    }
}