/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cache;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Map;

import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriBuilder;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.inmemory.InMemoryTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainer;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Ints;

import zone.src.sheaf.properties.ApplicationProperties;
import zone.src.sheaf.properties.PropertiesInfo;
import zone.src.sheaf.web.header.Header;

public class CacheControlFeatureTest {
    static String WEB_CONNECTOR_PORT = "${_}web.connector.${connector>.}port";
    static String WEB_CONNECTOR_INSECURE = "${_}web.connector.${connector>.}insecure";
    static int DEFAULT_INSECURE_PORT = 8080;
    private static final int PORT = getDefaultPort(ApplicationProperties.PROPS, true);
    
    //Tests that don't need deployment
    @Nested
    public class SimpleTests {
        protected void runTestContainer(CacheControlFeature cacheControlFeature) {
            DeploymentContext context = DeploymentContext.builder(getApplication(cacheControlFeature, VaryWithoutCacheStub.class)).build();
            TestContainer testContainer = new InMemoryTestContainerFactory().create(UriBuilder.fromUri("http://localhost/").port(PORT).build(), context);
            try {
                testContainer.start();
            } finally {
                testContainer.stop();
            }
        }
        
        @Test public void initialize() {
            //A @Vary header but no Cache-Control from annotation or default will throw:
            assertThatThrownBy(() -> {runTestContainer(new CacheControlFeature());}).isInstanceOf(IllegalStateException.class);

            //A @Vary header is accepted when there is a default Cache-Control but no annotations for Cache-Control:
            //  (just check this one doesn't throw)
            runTestContainer(new CacheControlFeature(new CacheControl()));
        }
        
        @Test public void construct() {
            assertThat(new CacheControlFeature().hasDefault()).isFalse();
            assertThat(new CacheControlFeature(null).hasDefault()).isFalse();
            assertThat(new CacheControlFeature(new CacheControl()).hasDefault()).isTrue();
        }
        
        @Test public void incorrectVary() {
            assertThatThrownBy(() -> {CacheControlFeature.validateVary(SimpleTests.class.getMethod("incorrect1"));}).isInstanceOf(IllegalArgumentException.class);
            assertThatThrownBy(() -> {CacheControlFeature.validateVary(SimpleTests.class.getMethod("incorrect2"));}).isInstanceOf(IllegalArgumentException.class);            
        }
        
        @Vary("multiple,fields") public void incorrect1() {
            //dummy method
        }
        @Vary(" whitespace") public void incorrect2() {
            //dummy method
        }
    }

    //Tests with a deployment of the feature without default for non-annotated classes
    @Nested
    public class NoDefaultCacheControlFeatureTest extends JerseyTest {
        @BeforeEach
        public void setUp() throws Exception {
            super.setUp();
        }
        @AfterEach
        public void tearDown() throws Exception {
            super.tearDown();
        }
        
        
        @Override
        protected TestContainerFactory getTestContainerFactory() {
            return new InMemoryTestContainerFactory();
        }

        @Override
        protected Application configure() {
            return getApplication(CacheControlFeature.class, AnnotatedStub.class, NonAnnotatedStub.class);
        }
        
        

        @Test public void headers() {
            assertCache("private, max-age=10000", HttpMethod.GET, target(AnnotatedStub.PATH));
            assertCache(null, HttpMethod.GET, target(NonAnnotatedStub.PATH));
            assertCache("no-cache", HttpMethod.PUT, target(AnnotatedStub.PATH), Entity.text(""));
            assertCache(null, HttpMethod.PUT, target(NonAnnotatedStub.PATH), Entity.text(""));
            assertCache("no-store", HttpMethod.DELETE, target(AnnotatedStub.PATH));
            assertCache(null, HttpMethod.DELETE, target(NonAnnotatedStub.PATH));
            assertCache(null, HttpMethod.POST, target(AnnotatedStub.PATH)); // empty @Cache annotation is equal to no @Cache annotation
            assertCache(null, HttpMethod.POST, target(NonAnnotatedStub.PATH));
        }

        @Test public void dontOverwriteExistingHeaders() {
            assertCache(200, "no-cache", HttpMethod.GET, target(AnnotatedStub.PATH), queryFor("no-cache"));
            assertCache(200, "proxy-revalidate", HttpMethod.GET, target(NonAnnotatedStub.PATH), queryFor("proxy-revalidate"));
            assertCache(200, "no-store", HttpMethod.POST, target(AnnotatedStub.PATH), queryFor("no-store"));
            assertCache(200, "no-cache", HttpMethod.POST, target(NonAnnotatedStub.PATH), queryFor("no-cache"));
        }

        @Test public void validFor() {
            assertCache("private, max-age=10000", HttpMethod.GET, target(AnnotatedStub.PATH));
            assertCache(202, null, HttpMethod.GET, target(AnnotatedStub.PATH), queryFor(202)); //no cache-control for 202
            assertCache(301, "private, max-age=20000", HttpMethod.GET, target(AnnotatedStub.PATH), queryFor(301));

            assertCache("no-cache", HttpMethod.PUT, target(AnnotatedStub.PATH), Entity.text(""));
            assertCache(202, null, HttpMethod.PUT, target(NonAnnotatedStub.PATH), Entity.text(""), queryFor(202));
            assertCache(202, "proxy-revalidate", HttpMethod.PUT, target(NonAnnotatedStub.PATH), Entity.text(""), queryFor(202, "proxy-revalidate"));
        }

        @Test public void vary() {
            //Vary from Cache annotation:
            assertCache(200, "private, max-age=10000", "Content-Type,x-y-z", HttpMethod.GET, target(AnnotatedStub.PATH_WITH_VARY), null, queryFor(200));
            assertCache(301, "private", "Content-Type,x-y-z", HttpMethod.GET, target(AnnotatedStub.PATH_WITH_VARY), null, queryFor(301));
            //Combined Vary from Cache annotation and response:
            assertCache(200, "private, max-age=10000", "x-1,x-2,x-3,Content-Type,x-y-z", HttpMethod.GET, target(AnnotatedStub.PATH_WITH_VARY), null,
                    ImmutableMap.of(AnnotatedStub.PARM_VARY, new String[] {"x-1,x-2", "x-3"}));
            //Don't duplicate existing Vary headers:
            assertCache(200, "private, max-age=10000", "Content-Type,x-1,x-y-z", HttpMethod.GET, target(AnnotatedStub.PATH_WITH_VARY), null,
                    ImmutableMap.of(AnnotatedStub.PARM_VARY, new String[] {"Content-Type,x-1"}));
            //Don't duplicate existing Vary headers [case insensitive]:
            assertCache(200, "private, max-age=10000", "content-type,x-1,x-y-z", HttpMethod.GET, target(AnnotatedStub.PATH_WITH_VARY), null,
                    ImmutableMap.of(AnnotatedStub.PARM_VARY, new String[] {"content-type,x-1"}));
        }
        
        @Test public void extension() {
            assertCache(201, "private, public, ext1=value", null, HttpMethod.GET, target(AnnotatedStub.PATH), null, queryFor(201));
        }
    }

    //Tests with a deployment of the feature with default for non-annotated classes
    @Nested
    public class WithDefaultCacheControlFeatureTest extends JerseyTest {
        @BeforeEach
        public void setUp() throws Exception {
            super.setUp();
        }
        @AfterEach
        public void tearDown() throws Exception {
            super.tearDown();
        }
        
        @Override protected TestContainerFactory getTestContainerFactory() {
            return new InMemoryTestContainerFactory();
        }

        @Override protected Application configure() {
            CacheControl defaultCacheControl = new CacheControl();
            defaultCacheControl.setPrivate(true);
            return getApplication(new CacheControlFeature(defaultCacheControl), AnnotatedStub.class, NonAnnotatedStub.class);
        }

        @Test public void headers() {
            assertCache("private, max-age=10000", HttpMethod.GET, target(AnnotatedStub.PATH));
            assertCache("private, no-transform", HttpMethod.GET, target(NonAnnotatedStub.PATH)); // the default cache-control provided in configure()

            assertCache("no-cache", HttpMethod.PUT, target(AnnotatedStub.PATH), Entity.text(""));
            assertCache(null, HttpMethod.PUT, target(NonAnnotatedStub.PATH), Entity.text(""));

            assertCache(null, HttpMethod.POST, target(AnnotatedStub.PATH)); // empty @Cache annotation is equal to no @Cache annotation
            assertCache(null, HttpMethod.POST, target(NonAnnotatedStub.PATH));
        }

        @Test public void dontOverwriteExistingHeaders() {
            assertCache(200, "no-cache", HttpMethod.GET, target(AnnotatedStub.PATH), queryFor("no-cache"));
            assertCache(200, "no-store, proxy-revalidate", HttpMethod.GET, target(NonAnnotatedStub.PATH), queryFor("no-store, proxy-revalidate"));
        }
    }

    private static Application getApplication(Object... components) {
        ResourceConfig app = new ResourceConfig();
        for (Object component: components) {
            if (component instanceof Class<?>)
                app.register((Class<?>)component);
            else
                app.register(component);
        }
        return app;
    }

    private static Map<String, String[]> queryFor(String cacheControl) {
        return ImmutableMap.of(AnnotatedStub.PARM_CACHE, new String[] {cacheControl});
    }

    private static Map<String, String[]> queryFor(int status) {
        return ImmutableMap.of(AnnotatedStub.PARM_STATUS, new String[] {String.valueOf(status)});
    }

    private static Map<String, String[]> queryFor(int status, String cacheControl) {
        return ImmutableMap.of(AnnotatedStub.PARM_STATUS, new String[] {String.valueOf(status)},
                AnnotatedStub.PARM_CACHE, new String[] {cacheControl});
    }

    private static void assertCache(String expectedCacheControl, String method, WebTarget target) {
        assertCache(200, expectedCacheControl, null, method, target, null, null);
    }

    private static void assertCache(int expectedStatus, String expectedCacheControl, String method, WebTarget target, Map<String, String[]> queryParameters) {
        assertCache(expectedStatus, expectedCacheControl, null, method, target, null, queryParameters);
    }

    private static void assertCache(String expectedCacheControl, String method, WebTarget target, Entity<?> entity) {
        assertCache(200, expectedCacheControl, null, method, target, entity, null);
    }

    private static void assertCache(int expectedStatus, String expectedCacheControl, String method, WebTarget target, Entity<?> entity,
                                    Map<String, String[]> queryParameters) {
        assertCache(expectedStatus, expectedCacheControl, null, method, target, entity, queryParameters);
    }

    private static void assertCache(int expectedStatus, String expectedCacheControl, String expectedVaryHeader, String method,
                                    WebTarget target, Entity<?> entity, Map<String, String[]> queryParameters) {
        if (queryParameters != null)
            for (String parm: queryParameters.keySet())
                target = target.queryParam(parm, (Object[])queryParameters.get(parm));
        Invocation.Builder responseBuilder = target.request();
        Response response = responseBuilder.method(method, entity);
        assertThat(response.getHeaderString(Header.CACHE_CONTROL)).isEqualTo(expectedCacheControl);
        assertThat(response.getStatus()).isEqualTo(expectedStatus);
        assertThat(response.getHeaderString(Header.VARY)).isEqualTo(expectedVaryHeader);
    }
    
    private static int getDefaultPort(PropertiesInfo properties, boolean insecure) {
      return Optional.fromNullable(Ints.tryParse(Strings.nullToEmpty(properties.get(WEB_CONNECTOR_PORT))))
          .or(DEFAULT_INSECURE_PORT);
    }
}
