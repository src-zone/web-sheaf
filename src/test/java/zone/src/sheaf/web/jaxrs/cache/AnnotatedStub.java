/*
 * Copyright 2015 by The.Src.Zone (Amsterdam, The Netherlands)  
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except 
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://the.src.zone/licenses/apache-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License 
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package zone.src.sheaf.web.jaxrs.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

import zone.src.sheaf.web.header.Header;

@Path(AnnotatedStub.PATH)
public class AnnotatedStub {
    static final String PARM_CACHE = "cache";
    static final String PARM_STATUS = "status";
    static final String PARM_VARY = "vary";
    static final String PATH = "annotated";
    static final String PATH_VARY = "vary";
    static final String PATH_WITH_VARY = PATH + "/" + PATH_VARY;

    private @Inject UriInfo uriInfo;

    @GET
    @Cache(isPrivate = true, maxAge = 10000, validFor = {200, 410})
    @Cache(isPrivate = true, maxAge = 20000, validFor = 301)
    @Cache(isPrivate = true, extensions = {"public", "ext1=value"}, validFor = 201)
    public Response get() {
        return response(uriInfo);
    }
    
    @Path(PATH_VARY) @GET
    @CacheConfiguration
    public Response getWithVary() {
        return response(uriInfo);
    }

    @PUT
    @Cache(isNoCache = true, validFor = {200})
    public Response put(String entity) {
        return response(uriInfo);
    }

    @DELETE
    @Cache(isNoStore = true)
    public Response delete() {
        return response(uriInfo);
    }

    @POST
    @Cache
    public Response post() {
        return response(uriInfo);
    }

    static Response response(UriInfo uriInfo) {
        String cacheControl = uriInfo.getQueryParameters().getFirst(PARM_CACHE);
        String status = uriInfo.getQueryParameters().getFirst(PARM_STATUS);
        List<String> varyFields = uriInfo.getQueryParameters().get(PARM_VARY);
        Response.ResponseBuilder response = Response.status(status == null ? 200 : Integer.valueOf(status));
        if (cacheControl != null)
            response = response.cacheControl(CacheControl.valueOf(cacheControl));
        if (varyFields != null)
            for (String varyField: varyFields)
                response.header(Header.VARY, varyField);
        return response.build();
    }
    
    @Cache(isPrivate = true, maxAge = 10000, validFor = {200, 410})
    @Cache(isPrivate = true, validFor = 301)
    @Vary({"Content-Type", "x-y-z"})
    @Target({ ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
    @Retention(value = RetentionPolicy.RUNTIME)
    @Inherited
    public @interface CacheConfiguration {
    }
}
